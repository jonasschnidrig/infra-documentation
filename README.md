# Energy System Infrastructure Characterisation

## General

This repository contains the `RMarkdown` files with corresponding `html` reports, the bibliography files with the corresponding notes of identified and used values.

## Abstract

The transition of the global energy sector from being fossil fuel-based towards a zero-carbon system is a necessity to mitigate the impacts of climate change. The ratification of both, the Paris agreement in 2015 as well as the European Green Deal in 2019, illustrates the political will to reach climate neutrality. Thereby, one major focus lies on the development of a power section with the aim to include a high share of renewable sources, while using the synergies of other energy vectors for conversion, storage and transportation. However, a successful decarbonisation of the energy sector is only achievable if the energy policies are accepted and implemented by the corresponding actors and stakeholders. The question arises: How can the power system in symbiosis to other energy vectors safely include the required amount of renewable energies ?

The aim of this paper is to develop a Mixed Integer Linear Programming (MILP) modelling methodology, helping to answer this question. The proposed methodology allows to model the role of energy independence in future multienergy distribution systems at different scales. On the basis of the current infrastructure, the inter-cell exchanges of the network will be categorised in terms of efficiency and capacity. The main exchanges of electricity, natural gas and hydrogen and, where appropriate, biomass and biomass and waste will also be represented.
This methodology will identify on how global energy system modelling can help national and international decision-makers at different levels of the society to understand and assess the impact of infrastructure on the energy system, answering the question of levels of centralization/decentralization (storage capacity, renewable production e.g) and grids capacity at various system levels depending on energy system objective functions (e.g overall investment costs, CO2 emission targets and import/export targets)

## Authors and acknowledgment

### Main author
- Jonas Schnidrig mailto:jonas.schnidrig@epfl.ch

### Contributing authors
- Xiang Li mailto:xiang.li@epfl.ch
- Ermanno Lo Cascio mailto:ermmano.locascio@epfl.ch
- François Maréchal mailto:francois.marechal@epfl.ch


## License
Open source, under the constraint of correct citation.

## Project status
The project is in WIP status and sees irregular updates.
