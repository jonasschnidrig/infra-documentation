---
title: "Hydrogen Gas Grid - Basic Expander"
author: jonasschnidrig
date: '`r format(Sys.Date(), "%B %d, %Y")`'
bibliography: gasgrid.bib
always_allow_html: true
output:
 bookdown::'html_document2':
    number_sections: yes  ##  Creates section (chapters)
    theme: simplex
    toc: yes
    toc_depth: 3
    toc_float: yes
---


***

![](images/Logo_EPFL.png){#id .class width=25%} ![](images/logo_ipese.png){#id .class width=25%}

***

**Author :** Jonas Schnidrig
**Version :** 0.0
**Academic validation by :**
**Industrial validation by :**

***


```{r, echo=FALSE}
print_code = FALSE ##  change option here if you want to print the code in the output document
```

```{r firstRun, message=FALSE, echo = print_code}
knitr::opts_chunk$set(message = FALSE) ##  desactivates messages printing
knitr::opts_chunk$set(warning = FALSE) ##  desactivates warnings printing

##  ----------- Install the packages of the libraries if they need to be installed
source("functions/init.R")
if(!require('tidyr'))
  install.packages("tidyr", dependencies = T)
library("BibDataManagement")
library("dplyr")
library("plotly")
library("knitr")
library("parcoords")
library("DT")
library("kableExtra")
if(!require('bibtex'))
  install.packages("bibtex", dependencies = T)
if(!require('openxlsx'))
  install.packages("openxlsx", dependencies = T)
if(!require('janitor'))
  install.packages("janitor", dependencies = T)
if(!require('tibble'))
  install.packages("tibble", dependencies = T)

```

#  Basic expander
The traditional expander corresponds of a mechanical Joule-Thomson expansion valve, a control valve, and a preheater to increase the gas temperature to 55°C, avoiding to generate hydrites after the isenthalpic expansion.


##  Efficiency

The basic expander can be modelled as a preheater and an isenthalpic valve, with following assumptions
- Ambient temperatures at the entry and exit of the expander
- Isenthalpic valve expansion
- No losses
- Heat compensation by the pre-heater
- 100% Methane
- Constant pressure drops (70-25, 25-5, 5-0.05 [bar])

With these assumptions, the heat load necessary has been simulated using ASPEN simulation software with an input of 1 kg/s of hydrogen.

The reference flow of  1kg/s can be converted into a Power input by taking the from VALI calculated lower heating value $LHV=120 \mathrm{MJ/kg}$, leading to a power entry of $120 \mathrm{MW}$.
We model the expanders without preheating, as the formation of hydrites can be neglected in the operating range of hydrogen in the operation region.

##  Reference size

```{r, echo = F}
frac_v_h2_ng <- 35.8/10.8
```

The reference size of the expanders can be defined by assuming a similar hydrogen grid structure in Switzerland as in Italy. The SNAM [@snamTransportationCapacityRedelivery2020] tracks the transported volume for all distribution points for natural gas. The conversion of the transported power can be calculated using the fraction of lower heating values of hydrogen and methane, leading to `r frac_v_h2_ng`

The size of expanders depends on the pipeline flow and pressure. Taking the maximum flow according to the pipeline specifications, it is possible to calculate the expander reference size.
```{r, echo=F}
expanders <- c('EHP/HP','HP/MP','MP/LP')
df <- read.xlsx("data/snam_grid.xlsx",sheet=2) %>%
   dplyr::slice(6:n()) %>%
   dplyr::select(comune=3, region=5, capacity=8, pressure=12) %>%
   dplyr::mutate(capacity=as.numeric(capacity), pressure = as.numeric(pressure)) %>%
   dplyr::group_by(pressure = cut(pressure, breaks = c(0,1,10,40,100))) %>%
   dplyr::summarise(capacity=mean(capacity)) %>%
   dplyr::ungroup() %>%
   dplyr::slice(1:(n()-2))
ref_size <- rev(df$capacity/1000/24)
exp1 <- data.frame(expanders,ref_size)
exp3 <- exp1 %>%
        dplyr::select(expanders, ref_size)
exp3$ref_size <- exp1$ref_size/frac_v_h2_ng
exp3 %>%
  kbl(col.names = c("Expander", 'Reference size [MW]')) %>%
  kable_paper("hover", full_width = F)
```


##  Lifetime & Cost
According to Berger [@rolandbergerDevelopmentBusinessCases2017], the lifetime and cost of for ehp/hp and hp/mp expansion stations can be estimated such as, with the maintenance costs corresponding to $8\%$ of the capex.
```{r, echo=F}
exp3$lifetime <- c(35,35,35)
exp3$capex <- c(0.7,0.6,0.4)/exp3$ref_size
exp3$opex <- 0.08*exp3$capex
exp3 %>%
  kbl(col.names = c("Expander", 'Reference size [MW]', 'Lifetime [years]', 'Cinv [M€]/GW', 'Cmaint [M€]/GW' )) %>%
  kable_paper("hover", full_width = F)
```

# References