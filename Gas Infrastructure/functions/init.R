# ----------- Install the packages of the libraries if they need to be installed

    if(!require('bookdown'))  install.packages("bookdown")
    if(!require('git2r'))  install.packages("git2r")
    if(!require('askpass'))  install.packages("askpass")
    if(!require('devtools'))  install.packages("devtools")
    
    if(!require('parcoords')) # <- for parallel coordinates
        install.packages("parcoords")
    if(!require('DT')) # <- for interactives data tables
        install.packages("DT")
    if(!require('dplyr'))  # <- for piping operator, code stuff to next line using %>%
        install.packages("dplyr", dependencies = T)
    if(!require('plotly')) # vor plotly graphics
        install.packages("plotly", dependencies = T)
    if(!require('knitr'))
        install.packages("knitr", dependencies = T)
    if(!require('kableExtra'))
        install.packages("kableExtra")
    if(!require('webshot'))# Enables to create pdf's with plotly
        install.packages('webshot')
    webshot::install_phantomjs()
    
    if (!require("BibDataManagement")){
        creds <- git2r::cred_user_pass('gitlab+deploy-token-57', 'xLK-v7jsQJ2sxPM2asfq')
        devtools::install_git("https://gitlab.epfl.ch/ipese/energyscope/bibliography-management.git", credentials = creds)
        # If an error message about too many redirects shows up, change/actualize your password on: https://gitlab.epfl.ch/profile/password/edit
    }
