# Gas grid infrastructure Documentation

Documentation of the natural gas & hydrogen grid modification for EnergyScope with related new technologies (expanders & compressors).

# Metadata
## Author
Jonas Schnidrig jonas.schnidrig@epfl.ch

## Dates
Created: 08.04.2021
Modified: 13.07.2021


