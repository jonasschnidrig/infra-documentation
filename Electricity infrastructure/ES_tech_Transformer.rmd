---
title: "Voltage EnergyScope - Transformer"
author: jonasschnidrig
date: '`r format(Sys.Date(), "%B %d, %Y")`'
bibliography: EnergyScope_Voltage4.bib
always_allow_html: true
output:
 bookdown::'html_document2':
    number_sections: yes  # Creates section (chapters)
    theme: simplex
    toc: yes
    toc_depth: 3
    toc_float: yes
---


***

![](images/Logo_EPFL.png){#id .class width=25%} ![](images/logo_ipese.png){#id .class width=25%}

***

**Author :** Jonas Schnidrig
**Version :** 0.1
**Academic validation by :** Rahul Kumar Gupta
**Industrial validation by :** 

***

```{r, echo=FALSE}
print_code = FALSE # change option here if you want to print the code in the output document
```

```{r firstRun, message=FALSE, echo = print_code}
knitr::opts_chunk$set(message = FALSE) # desactivates messages printing
knitr::opts_chunk$set(warning = FALSE) # desactivates warnings printing

# ----------- Install the packages of the libraries if they need to be installed
source("functions/init.R")
if(!require('tidyr'))
  install.packages("tidyr", dependencies = T)


library("BibDataManagement")
library("dplyr")
library("plotly")
library("knitr")
library("parcoords")
library("DT")
library("kableExtra")
library("bibtex")
library("openxlsx")
library('janitor')
library('tibble')
```



```{r, echo=FALSE}
dat_file <- "./EnergyScope_Voltage.bib"
grid <- readbibdata(dat_file, "
+- grid
V\\_EH\\_min = 220 [kV] # Minimum value Extra-High-Voltage (electricity from power plants and from abroad to transmission grid)
V\\_EH\\_max = 380 [kV] # Maximum value Extra-High-Voltage (electricity from power plants and from abroad to transmission grid)
V\\_H\\_min = 36 [kV] # Minimum value High-Voltage
V\\_H\\_max = 150 [kV] # Maximum value High-Voltage
V\\_M\\_min = 1 [kV] # Minimum value Medium-Voltage
V\\_M\\_max = 36 [kV] # Maximum value Medium-Voltage
V\\_L\\_max = 1 [kV] # Maximum value low-Voltage (Voltage to reach power sockets of households)
+-/grid
")

dat_file2 <- "./EnergyScope_Voltage2.bib"
grid <- readbibdata(dat_file2,
                    "
                    +- grid
                    EHV\\_Vmin = 220 [kV] # Minimum value Extra-High-Voltage (electricity from power plants and from abroad to transmission grid)
                    EHV\\_Vmax = 380 [kV] # Maximum value Extra-High-Voltage (electricity from power plants and from abroad to transmission grid)
                    HV\\_Vmin = 36 [kV] # Minimum value High-Voltage
                    HV\\_Vmax = 150 [kV] # Maximum value High-Voltage
                    MV\\_Vmin = 1 [kV] # Minimum value Medium-Voltage
                    MV\\_Vmax = 36 [kV] # Maximum value Medium-Voltage
                    LV\\_Vmax = 1 [kV] # Maximum value low-Voltage (Voltage to reach power sockets of households
                    +-/grid")

dat_file1 <- "./EnergyScope_Voltage1.bib"
grid <- readbibdata(dat_file1, "
+- grid
EHVVmin = 220 [kV] # Minimum value Extra-High-Voltage (electricity from power plants and from abroad to transmission grid)
EHVVmax = 380 [kV] # Maximum value Extra-High-Voltage (electricity from power plants and from abroad to transmission grid)
HVVmin = 36 [kV] # Minimum value High-Voltage
HVVmax = 150 [kV] # Maximum value High-Voltage
MVVmin = 1 [kV] # Minimum value Medium-Voltage
MVVmax = 36 [kV] # Maximum value Medium-Voltage
LVVmax = 1 [kV] # Maximum value low-Voltage (Voltage to reach power sockets of households
+-/grid")

EUD <- readbibdata(dat_file, "
+- EUD
EUD\\_HH\\_ELEC = 67540 [TJ] # End use demand electricity households 2015
+-/EUD
")

trafo <- readbibdata(dat_file, "
+- eta
100kVA = 98.2 [%] #
50kVA = 97.9 [%] #
25kVA = 96.8 [%] #
10kVA = 96.1 [%] #
5kVA = 95.3 [%] #
+-/eta
")

trafo2 <- readbibdata(dat_file, "
+-lifetime
mean = 2391 [days]
min = 1955 [days]
max = 3360 [days]
design\\_HV = 65[years] #transmission trafos
design\\_LV = 75 [years] #distribution trafos
+-/lifetime
")

trafo3 <- readbibdata(dat_file, "
+-cinv
75kVA = 4098[$]
150kVA = 5152 [$]
225kVA = 6260 [$]
300kVA = 6720 [$]
500kVA = 8571 [$]
750kVA = 12395 [$]
1000kVA = 12298 [$]
+-/cinv
")

trafo4 <- readbibdata(dat_file,"
+-LCA
gwp = 3780 [kg CO2 eq / MVA]
+-/LCA
")
```

# Transformer (DRAFT/WIP)
A transformer is a component of electrical engineering. It usually consists of two or more coils (windings), usually made of insulated copper wire wound on a common magnetic core. A transformer transforms an AC input voltage applied to one of the coils into an AC output voltage that can be tapped at the other coil.
Within EnergyScope, transformers are used to switch between voltage levels.

The following parameter estimation is a first attempt and needs further work

## Efficiency
The efficiency of the transformer versus its charge can be expressed in function of the different power sized of the transformer. Sumon [@sumonComputerAidedDesign2015] lists 5 different transformer sizes with the respective efficiency. Latter efficiency varies between `r trafo$'5kVA'$value` & `r trafo$'100kVA'$value` % or a relative variation of `r round((trafo$'100kVA'$value-trafo$'5kVA'$value)/trafo$'100kVA'$value*1e4,0)/100` %.

```{r, echo=F}
eta <- c(trafo$`5kVA`$value, trafo$`10kVA`$value, trafo$`25kVA`$value, trafo$`50kVA`$value, trafo$`100kVA`$value)
power <- c(50, 100, 250, 500, 1000)
ggplot(data.frame(eta, power), aes(power, eta)) +
  geom_point() +
  #stat_smooth(method = 'lm', aes(colour = 'linear'), se = FALSE) +
  stat_smooth(method = 'lm', formula = y ~ poly(x, 2), aes(colour = 'polynomial'), se = FALSE) +
  stat_smooth(method = 'nls', formula = y ~ a * log(x) + b, aes(colour = 'logarithmic'), se = FALSE, start = list(a = 1, b = 1)) +
  #stat_smooth(method = 'nls', formula = y ~ a*exp(b *x), aes(colour = 'Exponential'), se = FALSE) +
  theme_bw() +
  labs(y = expression("Efficiency" ~ eta ~ "[%]"),
       x = "Power [kVA]")
```

3 different types of transformers need to be integrated in EnergyScope, in order to switch between the voltage levels. We assume the efficiency of voltage up/down transformation being constant.

- EHV/HV & HV/EHV (transition level 2)
- HV/MV & MV/HV (transition level 4)
- MV/LV & LV/MV (transition level 6)

The respective efficiencies are
```{r, echo=F}
trafos <- c('EHV/HV', 'HV/MV', 'MV/LV')
eta <- c(trafo$'100kVA'$value, mean(c(trafo$'50kVA'$value, trafo$'25kVA'$value)), mean(c(trafo$'10kVA'$value, trafo$'5kVA'$value)))
dat_trafo <- data.frame(trafos, eta)
dat_trafo %>%
  kbl(col.names = c("Transformer", "Efficiency [%]")) %>%
  kable_paper("hover", full_width = F)
```
## Reference size
The reference size of the transformers is given by Sumon [@sumonComputerAidedDesign2015] and added to the the spec-sheet such as
```{r, echo=F}
dat_trafo$ref_size <- c(1000, 375, 75)
dat_trafo %>%
  kbl(col.names = c("Transformer", "Efficiency [%]", "Reference size [kW]")) %>%
  kable_paper("hover", full_width = F)
```
## Lifetime

According to Shimomugi et al. [@shimomugiHowTransformersAge2019] the lifetime of transformer depends on various stress factors such as

- Winding clamping force
- Short circuits
- Seismic resistance

A first attempt to estimate the lifetime of transformers subject to latter effects has been conducted by Osorio [@osorioTransformerLifetimePrediction2018]. He concluded that the expectancy varies between `r round(trafo2$min$value/365*100,0)/10` and `r round(trafo2$max$value/365*100,0)/10` years.

Electric power companies such as ABB or TEPCO guarantee their transformers to last between `r trafo2$'design\\_LV'$value` and `r trafo2$'design\\_HV'$value` `r trafo2$'design\\_HV'$unit`, according to the voltage level.

```{r, echo=F}
dat_trafo$lifetime <- c(trafo2$`design\\_HV`$value, mean(c(trafo2$`design\\_HV`$value, trafo2$`design\\_LV`$value)), trafo2$`design\\_LV`$value)
dat_trafo %>%
  kbl(col.names = c("Transformer", "Efficiency [%]", "Reference size [kW]", "Lifetime [years]")) %>%
  kable_paper("hover", full_width = F)
```
## Cost
The investment cost of a transformer is mainly composed of the material costs and the production & development costs. Zandt et al. [@zandtDistributionTransformersRepresent2004] estimated the investment and the costs of the losses of different distribution transformers according several levels. The results are visible in the following graph, where the decrease in specific cost is visible with higher levels.
```{r, echo=F}
cinv <- c(trafo3$'75kVA'$value, trafo3$'150kVA'$value, trafo3$'225kVA'$value, trafo3$'300kVA'$value, trafo3$'500kVA'$value, trafo3$'750kVA'$value, trafo3$'1000kVA'$value)
power <- c(75, 150, 225, 300, 500, 750, 1000)
ggplot(data.frame(cinv, power), aes(power, cinv)) +
  geom_point() +
  #stat_smooth(method = 'lm', aes(colour = 'linear'), se = FALSE) +
  stat_smooth(method = 'lm', formula = y ~ poly(x, 2), aes(colour = 'polynomial'), se = FALSE) +
  stat_smooth(method = 'nls', formula = y ~ a * log(x) + b, aes(colour = 'logarithmic'), se = FALSE, start = list(a = 1, b = 1)) +
  #stat_smooth(method = 'nls', formula = y ~ a*exp(b *x), aes(colour = 'Exponential'), se = FALSE) +
  theme_bw() +
  labs(y = "Investment cost [$/kVA]",
       x = "Power [kVA]")
```
EnergyScope requires the specific investment cost, calculated as
$$c_{inv}=\frac{C_{inv}{ref_{size}}$$
This allows us to determine the specific cost such as
```{r, echo=F}
dat_trafo$cinv <- round(c(trafo3$'1000kVA'$value / 1e6,
                          (trafo3$'300kVA'$value + 75 / 200 * (trafo3$'500kVA'$value - trafo3$'300kVA'$value)) / 375e3,
                          trafo3$'75kVA'$value / 75e3
) * 10000) / 10
dat_trafo %>%
  kbl(col.names = c("Transformer", "Efficiency [%]", "Reference size [kW]", "Lifetime [years]", "Investment cost [MCHF/GW]")) %>%
  kable_paper("hover", full_width = F)
```

The maintenance cost has been monitored by Azis et al. [@azisMaintenanceCostStudy2018] for (E)HV & MV types of power transformers in 2012/2013 and 2013/2014 in Malaysia.
```{r, echo = FALSE, fig.cap="", out.width="80%"}
knitr::include_graphics("images/cmaint_trafo.png")
```

As EnergyScope considers constant annual maintenance costs for an average operating time, latter data had to be integrated over the lifetime and divided by the the annually converted energy.
```{r, echo=F}
c_maint <- (35 * 40 + 20 * 20) / 60
dat_trafo$cmaint <- round(c_maint/(dat_trafo$ref_size*8760*0.9/1e3)*1e3)/1e3/4
dat_trafo %>%
  kbl(col.names = c("Transformer", "Efficiency [%]", "Reference size [kW]", "Lifetime [years]", "Investment cost [MCHF/GW]", "Maintenance cost [MCHF/GWh]")) %>%
  kable_paper("hover", full_width = F)
```


## GWP construction
The global warming potential has been determined by Hegedic et al. [@hegedicLIFECYCLEASSESSMENT2016] using a LCA approach. The production GWP amounts to `r trafo4$gwp$value` `r trafo4$gwp$unit`. This approach assumes the LCA constant for all transformer types. Latter can be integrated into the datasheet.
```{r, echo=F}
dat_trafo$gwp_constr <- trafo4$gwp$value/1e3
dat_trafo %>%
  kbl(col.names = c("Transformer", "Efficiency [%]", "Reference size [kW]", "Lifetime [years]", "Investment cost [MCHF/GW]", "Maintenance cost [MCHF/GWh]", "GWP construction [ktCO2-eq./GW]")) %>%
  kable_paper("hover", full_width = F)
```

# References
